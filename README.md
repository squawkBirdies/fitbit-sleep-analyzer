# README #

Fitbit Sleep Analyzer

![Image](ExampleScreenShot.png)

### Summary ###

This is a Python utility which is designed to download Fitbit sleep records for circadian rhythm research performed by the Lighting Enabled Systems & Applications Engineering Research Center (LESA ERC) at Rensselaer Polytechnic Institute. The Fitbit Sleep Analyzer handles user authentication with Fitbit's site in compliance with the API ToS. [LESA ERC main site](https://lesa.rpi.edu/)


### How do I get set up? ###
#### macOS, Linux: ####
1. Install [Python 2.7](https://www.python.org/download/releases/2.7/) or [Python 3.6](https://www.python.org/downloads/release/python-361/). Alternatively, use a package manager like brew or aptitude. 
2. Install [Google Chrome](https://www.google.com/chrome/index.html)
3. Install [requests-oauthlib](https://github.com/requests/requests-oauthlib) library
4. Install [Selenium](https://github.com/SeleniumHQ/selenium/tree/master/py) library
5. Install [Chrome WebDriver](https://sites.google.com/a/chromium.org/chromedriver/). Instructions for installing Chrome and the Webdriver on Ubuntu can be found [here](https://christopher.su/2015/selenium-chromedriver-ubuntu/). 
6. Install [cryptography-2.0](https://cryptography.io/en/latest/) library
7. (macOS 10.11 and up): Install [OpenSSL headers](https://solitum.net/openssl-os-x-el-capitan-and-brew/) via Brew;
   (Ubuntu): Install the OpenSSL headers using Aptitude as described [here](https://stackoverflow.com/questions/3016956/how-do-i-install-the-openssl-libraries-on-ubuntu). 
8. Install [SecureString](https://pypi.python.org/pypi/SecureString)

SecureString can be easily installed for Python 2 by running this command:
```
#!bash

pip install securestring
```

For Python 3, the custom version of SecureString hosted on this repository must be used. Navigate to the pysecstr folder and run:
```
#!bash

sudo pip3 install . 
```
Alternatively, the new version of SecureString hosted on this repository can actually be installed on Python 2 or 3; the package automatically configures the code for the environment. 

#### Windows 10: ####
Windows 10 users must run fitbitSleepNoSecureString.py because of incompatibilities between OpenSSL on Windows and the SecureString library. 

1. Install [Python 3.6](https://www.python.org/downloads/release/python-361/).
2. Add the Python directory path to the Windows environment variables. This is usually 'C:\Python36'. Add both 'C:\Python36' and 'C:\Python36\Scripts' paths. If this step is not performed, running the 'python' and 'pip' commands in the Windows CMD will result in an error. For more details, see [this question](https://stackoverflow.com/questions/36136000/cannot-install-pip-on-windows-10).
3. Install [Google Chrome](https://www.google.com/chrome/index.html)
3. Install [requests-oauthlib](https://github.com/requests/requests-oauthlib) library
4. Install [Selenium](https://github.com/SeleniumHQ/selenium/tree/master/py) library
5. Download the [Chrome WebDriver](https://sites.google.com/a/chromium.org/chromedriver/), unzip it, and place the .exe into the 'FitbitSleep' directory that the program configuration files will be stored in (see below). 
6. Install [cryptography-2.0](https://cryptography.io/en/latest/) library

#### All Platforms: ####
Before the first run, the program requires three blank .txt files in a folder named 'FitbitSleep' in the user's 'Documents' directory: ‘token.txt’, ‘salt.txt’, and 'nonce.txt'. Ensure that they are zero kilobytes in size before the first run. These will be managed by the program afterwards. In addition, OpenSSL with headers must be installed (not the same as installing OpenSSL). Then, navigate to the directory that the program is stored in and run:
```
#!bash

python fitbitSleep.py
```
Substitute 'python3' for 'python' if your platform requires it and 'fitbitSleepNoSecureString' for 'fitbitSleep' on platforms that do not support SecureString. 


### Compatibility ###
Full Compatibility: macOS, Ubuntu Linux

Limited Compatibility: Windows 10


### Fitbit Devices ###
To ensure correct functionality, the model name of the device linked to the user's account must be entered in the string 'device_type'. Note that newer models have more precise sleep tracking and that models with heart rate monitoring also tend to be more accurate. The following Fitbit devices have been tested with this program:

* [Fitbit Surge:](https://www.fitbit.com/surge) Use device_type = "Surge"
* [Fitbit Charge 2:](https://www.fitbit.com/charge2) Use device_type = "Charge 2"


### Packaging ###
This program has been successfully packaged into an executable by using [PyInstaller](http://www.pyinstaller.org/). This is done by running the following command:

```
#!bash

sudo pyinstaller -F fitbitSleep.py 
```

In order to recompile when a packaged binary already exists:

```
#!bash

sudo rm -rf build dist
sudo pyinstaller -F -w --icon=FitbitDownloaderIcon.icns fitbitSleep.py
```


### Integrity Checks ###
This program checks the integrity of its configuration files on each run before attempting to connect to the Fitbit servers. Please do not modify the files yourself; let the program manage them. If the program exits immediately upon starting, zero one of the configuration files to cause the program to re-download an access token and fix the files. Note that process of initially generating the anti-tamper parameters can take a few seconds on a modern computer. Integrity checks can be disabled by setting the 'difficulty' variable in the code to zero. Stronger anti-tampering protection can be achieved by setting 'difficulty' to a higher number at the cost of computational power to generate the anti-tamper parameter.  


### Contribution guidelines ###

Access to this software is currently limited to LESA personnel and other approved individuals. 


### Who do I talk to? ###

Contact LESA ERC (see site for contact details). Lawrence Fan is the project originator.


### Resources ###

[Fitbit API Documentation](https://dev.fitbit.com/)

[Brew:](https://brew.sh/) Useful for installing Chrome WebDriver on macOS.

[Proof that SecureString clears objects from memory](https://www.sjoerdlangkemper.nl/2016/06/09/clearing-memory-in-python/)

[*Chronobiology Meets Work Life* by Joana Leonie Matera](https://edoc.ub.uni-muenchen.de/18854/)


### Other Information ###
This is unofficial software, [Fitbit](https://www.fitbit.com/) does not endorse this project. Licensing terms pending on LESA ERC intellectual property manager. All rights reserved, Copyright 2017. This software is provided AS-IS without any warranty, express or implied, of its functionality, safety, or suitability for any use. For research use only. Not intended to diagnose or treat any medical condition.