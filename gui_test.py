from tkinter import Entry, Label, Toplevel, StringVar, OptionMenu, Button, Tk, W
from collections import OrderedDict
import datetime

def login_handler(ent):
	print(ent.get())

def get_date_handler(month, date, year):
	print(month.get())
	print(date.get())
	print(year.get())

root = Tk()
root.title("Fitbit Sleep Analyzer")
now = datetime.datetime.now()

top = Toplevel()
top.title("Login")
Label(top, text = "PIN: ").grid(row = 0, column = 0, sticky = W)
e = Entry(top, width = 10, show = '*')
e.grid(row = 0, column = 1, sticky = W)
b = Button(top, text = "Login", command = lambda: login_handler(e))
b.grid(row = 0, column = 2, sticky = W)
top.lift()

months = OrderedDict([('Jan', 0), ('Feb', 1), ('Mar', 2), ('Apr', 3), ('May', 4), ('Jun', 5), ('Jul', 6), ('Aug', 7), ('Sep', 8), ('Oct', 9), ('Nov', 10), ('Dec', 11)])
dates = OrderedDict([('1', 0), ('2', 1), ('3', 2), ('4', 3), ('5', 4), ('6', 5), ('7', 6), ('8', 7), ('9', 8), ('10', 9), ('11', 10), ('12', 11), ('13', 12), ('14', 13), ('15', 14), ('16', 15), ('17', 16), ('18', 17), ('19', 18), ('20', 19), ('21', 20), ('22', 21), ('23', 22), ('24', 23), ('25', 24), ('26', 25), ('27', 26), ('28', 27), ('29', 28), ('30', 29), ('31', 30)])
years = OrderedDict([('2014', 0), ('2015', 1), ('2016', 2), ('2017', 3), ('2018', 4), ('2019', 5), ('2020', 6)])

title_var = StringVar(root)
title_var.set("Status: Not logged in")
Label(root, textvariable = title_var).grid(row = 0, column = 0, columnspan = 6, sticky = W)
Label(root, text = "Date: ").grid(row = 1, column = 0, sticky = W)
month_var = StringVar(root)
month_var.set(list(months.items())[now.month - 1][0])
month_menu = OptionMenu(root, month_var, *months)
dates_var = StringVar(root)
dates_var.set(now.day)
dates_menu = OptionMenu(root, dates_var, *dates)
years_var = StringVar(root)
years_var.set(now.year)
years_menu = OptionMenu(root, years_var, *years)
download = Button(root, text = "Get", command = lambda: get_date_handler(month_var, dates_var, years_var)).grid(row = 1, column = 4)

month_menu.grid(row = 1, column = 1, sticky = W)
dates_menu.grid(row = 1, column = 2, sticky = W)
years_menu.grid(row = 1, column = 3, sticky = W)

Label(root, text = "Sleep Start: ").grid(row = 2, column = 0, sticky = W, columnspan = 2)
Label(root, text = "Sleep End: ").grid(row = 3, column = 0, sticky = W, columnspan = 2)
Label(root, text = "Midsleep: ").grid(row = 4, column = 0, sticky = W, columnspan = 2)
Label(root, text = "Total Sleep Time: ").grid(row = 5, column = 0, sticky = W, columnspan = 2)

root.mainloop()

