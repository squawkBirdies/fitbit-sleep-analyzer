#Lawrence Fan for Lighting Enabled Systems & Applications ERC, Copyright 2017
#Python script used to download Fitbit sleep records from API. Stores encrypted Oauth2 access token in a separate text file "token.txt" and password salt in "salt.txt". 
#If the token is invalid, the program opens up a separate Chrome tab, allows the user to log into Fitbit.com to authenticate, and overwrites the old token with the new one. 
#This is the Windows version of this program
#Removed use of SecureString library for compatibility with platforms which cannot compile it
#Requirements: Python 2.7 or 3.6, requests_oauthlib, selenium, Google Chrome browser, chromedriver, cryptography 2.0

import requests
import time
import datetime
import json
from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import MobileApplicationClient
from selenium import webdriver
import sys
import getpass
import gc
import base64
import os
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from tkinter import Entry, Label, Toplevel, StringVar, OptionMenu, Button, Tk, W, Message
from collections import OrderedDict

#Definitions
client_id = "228KRK"	#Insert Application ID from Fitbit App Manager here
device_type = "Charge 2"	#Name of Fitbit model owned by user
min_pin_length = 4		#Minimum password length
time_format = "%I:%M:%S %p"		#Some strftime codes do not work on Windows

#Program definitions
months = OrderedDict([('Jan', 0), ('Feb', 1), ('Mar', 2), ('Apr', 3), ('May', 4), ('Jun', 5), ('Jul', 6), ('Aug', 7), ('Sep', 8), ('Oct', 9), ('Nov', 10), ('Dec', 11)])
dates = OrderedDict([('1', 0), ('2', 1), ('3', 2), ('4', 3), ('5', 4), ('6', 5), ('7', 6), ('8', 7), ('9', 8), ('10', 9), ('11', 10), ('12', 11), ('13', 12), ('14', 13), ('15', 14), ('16', 15), ('17', 16), ('18', 17), ('19', 18), ('20', 19), ('21', 20), ('22', 21), ('23', 22), ('24', 23), ('25', 24), ('26', 25), ('27', 26), ('28', 27), ('29', 28), ('30', 29), ('31', 30)])
years = OrderedDict([('2014', 0), ('2015', 1), ('2016', 2), ('2017', 3), ('2018', 4), ('2019', 5), ('2020', 6)])	#Ensure that your current year is on this list
scope = ["activity", "heartrate", "location", "nutrition", "profile", "settings", "sleep", "social", "weight"]
client = MobileApplicationClient(client_id)

#Global variable definitions
global fitbit_object
fitbit_object = OAuth2Session(client_id, client=client, scope=scope)


#Functions
def load_config():
	#Attempt to load encrypted authorization token from token.txt file and decrypt it
	authentication = False
	token_loaded = False
	salt_loaded = False
	#Make sure that token.txt is present
	try:
		f = open('token.txt', 'r+b')		#Load ciphertext token
		token_loaded = True;
	except:
		token_loaded = False
	#Make sure that salt.txt is present
	try:
		k = open('salt.txt', 'r+b')		#Load ciphertext salt
		salt_loaded = True
	except:
		salt_loaded = False
	#If files are not found, print error messages
	if token_loaded == False:
		print("Error: Access token file not found!")
	if salt_loaded == False:
		print("Error: Salt value file not found!")
	if (token_loaded == False) or (salt_loaded == False):
		#If either file is missing, close the other file handler and exit the program
		print("Program exit due to missing configuration file.")
		try:
			f.close()
		except:
			pass
		try:
			k.close()
		except:
			pass
		gc.collect()	#Call garbage collector immediately before exit in an attempt to flush sensitive objects from memory
		sys.exit()
	else:
		return f, k

def pin_entry_handler(top, top2, e1, e2, f, k, callback_url, title_var):
	pin_1 = e1.get().encode()
	pin_2 = e2.get().encode()
	#Ensure that PINs meet minimum length requirements
	if (len(pin_1) >= min_pin_length) and (len(pin_2) >= min_pin_length):
		if pin_1 == pin_2:
			#Quick Crypto 101: salts are not secrets. They are designed to increase the randomness of keys derived from user-entered passwords, to slow down password cracking, and to thwart the use of precomputed password hashes ("rainbow tables")
			salt_bae = os.urandom(16)		#Random 128-bit salt to increase entropy of final key
			kdf = PBKDF2HMAC(algorithm=hashes.SHA256(), length=32, salt=salt_bae, iterations=100000, backend=default_backend())
			key = base64.urlsafe_b64encode(kdf.derive(pin_1))		#Derive key using HMAC algorithm
			fer = Fernet(key)
			ciphertxt = fer.encrypt(callback_url.encode())	#Encrypt callback URL containing OAuth2 token
			f.seek(0)	#Clear text from token file
			f.truncate()
			f.write(ciphertxt)	#Write encrypted callback URL
			k.seek(0)	#Clear text from salt file
			k.truncate()
			k.write(salt_bae)	#Write salt to file
			auth_complete = True
			fitbit_object.token_from_fragment(callback_url)
			try:
				r = fitbit_object.get('https://api.fitbit.com/1/user/-/profile.json')
				result = json.loads(r.content.decode())
				if 'user' not in result:
					#Received an authentication failure from Fitbit
					print("Cannot access Fitbit account. Check Internet connection.")
					f.close()
					k.close()
					gc.collect()	#Call garbage collector immediately before exit in an attempt to flush sensitive objects from memory
					sys.exit()
				else:
					#Successfully received user profile overview, proving that the token works
					title_var.set("Status: Logged in")	#Show to user that they are logged in
					top.destroy()	#Remove login window
			except:
				print("Cannot access Fitbit account. Check Internet connection.")
				f.close()
				k.close()
				gc.collect()	#Call garbage collector immediately before exit in an attempt to flush sensitive objects from memory
				sys.exit()
			top2.destroy()
		else:
			#Passwords do not match, clear them from RAM and warn user
			e1.delete(0, 'end')
			e2.delete(0, 'end')
			top3 = Toplevel()
			top3.title("Error")
			warn_str = "Passwords do not match!"
			Message(top3, text=warn_str).pack()
			Button(top3, text='Dismiss', command=top3.destroy).pack()
			top3.lift()
	else:
		top3 = Toplevel()
		top3.title("Error")
		warn_str = "Password does not meet minimum length!"
		Message(top3, text=warn_str).pack()
		Button(top3, text='Dismiss', command=top3.destroy).pack()
		top3.lift()

def authorize(root, top, f, k, title_var):
	# Grab the URL for Fitbit's authorization page.
	authorization_url = "https://www.fitbit.com/oauth2/authorize"
	auth_url, state = fitbit_object.authorization_url(authorization_url)
	driver = webdriver.Chrome()		#Open a remote-controlled Chrome window using Selenium	
	driver.get(auth_url)	#Open Fitbit authorization page in separate Chrome window to comply with Fitbit API ToS
	callback_url = ""
	auth_complete = False
	#Wait for user to type in credentials and log in
	while auth_complete == False:
		time.sleep(1)
		if str(driver.current_url).find("localhost") != -1:
			#Fitbit app profile is set up to callback to localhost after login; search for this and then copy URL containing Oauth2 token
			callback_url = str(driver.current_url)
			#Quit Chrome and overwrite invalid token with new, valid token. 
			driver.close()
			auth_complete = True
	#Prompt user to enter password to protect token (and therefore access to their Fitbit account)
	top2 = Toplevel()
	top2.title("Create PIN")
	min_length_alert = "Minimum length: " + str(min_pin_length)
	Label(top2, text = min_length_alert).grid(row = 0, column = 0, sticky = W)
	Label(top2, text="Create new PIN: ").grid(row = 1, column = 0, sticky = W)
	e1 = Entry(top2, width = 10, show = '*')
	e1.grid(row = 1, column = 1, sticky = W)
	Label(top2, text="Enter PIN again: ").grid(row = 2, column = 0, sticky = W)
	e2 = Entry(top2, width = 10, show = '*')
	e2.grid(row = 2, column = 1, sticky = W)
	b = Button(top2, text = "Enter", command = lambda: pin_entry_handler(top, top2, e1, e2, f, k, callback_url, title_var))
	b.grid(row = 3, column = 1, sticky = W)
	top2.lift()
	#Prevent root window from closing and exiting program until the user has entered their new password
	root.wait_window(top2)

def login_handler(root, ent, top, title_var, f, k, token_salt, test_token_cipher):
	correct_password = False
	pin = ent.get().encode()
	kdf = PBKDF2HMAC(algorithm=hashes.SHA256(), length=32, salt=token_salt, iterations=100000, backend=default_backend())
	key = base64.urlsafe_b64encode(kdf.derive(pin))		#Derive decryption key from salt and user response
	fer = Fernet(key)	#Create Fernet object (Note that Fernet is based on AES-128 and SHA256)
	try:
		test_decrypt = fer.decrypt(test_token_cipher)	#If the password is incorrect, this call will raise an exception
		correct_password = True 	#This code is only reachable when the decryption is successful
	except:
		#Incorrect key results in an exception handled here
		e.delete(0, 'end')
		top2 = Toplevel()
		top2.title("Error")
		msg = Message(top2, text="Incorrect password")
		msg.pack()
		button = Button(top2, text='Dismiss', command=top2.destroy)
		button.pack()
		top2.lift()
	authentication = False
	if correct_password == True:
		#Decrypt access token	
		fitbit_object.token_from_fragment(fer.decrypt(test_token_cipher).decode())	#Extract OAuth2 token from decrypted URL
		#Request the user profile from Fitbit API to test the stored token
		try:
			r = fitbit_object.get('https://api.fitbit.com/1/user/-/profile.json')
			result = json.loads(r.content.decode())
			if 'user' not in result:
				#Received an authentication failure from Fitbit
				authentication = False
				authorize(root, top, f, k, title_var)
			else:
				#Successfully received user profile overview, proving that the token works
				authentication = True
				title_var.set("Status: Logged in")
				top.destroy()
		except:
			print("Cannot access Fitbit account. Check Internet connection.")
			f.close()
			k.close()
			gc.collect()	#Call garbage collector immediately before exit in an attempt to flush sensitive objects from memory
			sys.exit()

def get_date_handler(month, date, year, start_var, end_var, midsleep_var, duration_var):
	#Get data and parse it in this function
	month_int = months[month.get()] + 1
	queryURL = "https://api.fitbit.com/1.2/user/-/sleep/date/" + year.get() + "-" + str(month_int) + "-" + date.get() + ".json"	#Generate JSON access URL to request the data
	try:
		sleepDate = datetime.date(int(year.get()), int(month_int), int(date.get()))
	except ValueError:
		#User has entered in an invalid date (ex. Feb. 31st)
		top2 = Toplevel()
		top2.title("Warning")
		warn_str = "Invalid date!"
		msg = Message(top2, text=warn_str) 
		msg.pack()
		button = Button(top2, text='Dismiss', command=top2.destroy)
		button.pack()
		top2.lift()
		return

	sleepDateChk = datetime.datetime(int(year.get()), int(month_int), int(date.get()), 12, 0)	#Fitbit must have synced after this date and time
	r = fitbit_object.get(queryURL)
	result = json.loads(r.content.decode())

	queryDevices = "https://api.fitbit.com/1/user/-/devices.json"	#Get user's devices
	dev = fitbit_object.get(queryDevices)
	devices = json.loads(dev.content.decode())
	try:
		if device_type != devices[0]['deviceVersion']: 
			top2 = Toplevel()
			top2.title("Warning")
			msg = Message(top2, text="Records for device not found!")		#Alert user if device is not found in user's account 
			msg.pack()
			button = Button(top2, text='Dismiss', command=top2.destroy)
			button.pack()
			top2.lift()
		else:
			#Parse time of last sync
			lastSyncStr = devices[0]['lastSyncTime']
			Ld, Lt = lastSyncStr.split('T')
			Lye, Lmo, Lda = Ld.split('-')
			Lho, Lmi, Lse = Lt.split(":")
			lastSync = datetime.datetime(int(Lye), int(Lmo), int(Lda), int(Lho), int(Lmi))
			#Alert user if device last synced before requested sleep date
			if lastSync < sleepDateChk:
				warn_str = "Device may have last synced before requested date!\n"
				warn_str += "Device last synced at: " + str(lastSync)
				top2 = Toplevel()
				top2.title("Warning")
				msg = Message(top2, text=warn_str) 
				msg.pack()
				button = Button(top2, text='Dismiss', command=top2.destroy)
				button.pack()
				top2.lift()

		numSleepRecords = result['summary']['totalSleepRecords']
		if int(numSleepRecords) > 0:
			#Need to make sure that the user actually slept that day
			sleepStartStr = result['sleep'][0]['startTime']		#Get start time of sleep
			#Parse Fitbit time string into Python datetime object
			Sd, St = sleepStartStr.split('T')
			Sye, Smo, Sda = Sd.split('-')
			Sho, Smi, Sse = St.split(":")
			Sse, Sms = Sse.split(".")
			sleepStart = datetime.datetime(int(Sye), int(Smo), int(Sda), int(Sho), int(Smi), int(Sse))
			start_var.set(str(sleepStart.strftime(time_format)))	#Print formatted time string
			sleepEndStr = result['sleep'][0]['endTime']		#Get end time of sleep

			#Parse Fitbit time string into Python datetime object
			d, t = sleepEndStr.split('T')
			ye, mo, da = d.split('-')
			ho, mi, se = t.split(":")
			se, ms = se.split(".")
			sleepEnd = datetime.datetime(int(ye), int(mo), int(da), int(ho), int(mi), int(se))
			end_var.set(str(sleepEnd.strftime(time_format)))

			timeSleep = sleepEnd - sleepStart 	#Use timeDelta to calculate approximate sleep length
			duration_var.set(str(timeSleep))
			timeSleepSeconds = timeSleep.total_seconds()
			midsleepSeconds = timeSleepSeconds/2 	#Calculate number of seconds between start of sleep and midsleep. Python 3 introduces automatic int to float, but truncating 0.5 seconds in Python 2 doesn't really matter
			midsleepDelta = datetime.timedelta(seconds=midsleepSeconds)
			midsleep = sleepStart + midsleepDelta 	#Calculate midsleep time in hours and minutes
			midsleep_var.set(str(midsleep.strftime(time_format)))
		else:
			#User did not sleep that day or records are otherwise missing
			start_var.set("")
			end_var.set("")
			midsleep_var.set("")
			duration_var.set("")
			top2 = Toplevel()
			top2.title("Warning")
			msg = Message(top2, text="No sleep recorded on this date")
			msg.pack()
			button = Button(top2, text='Dismiss', command=top2.destroy)
			button.pack()
			top2.lift()
	except:
		#Alert user that json download from Fitbit server has failed, probably because of login issues
		top2 = Toplevel()
		top2.title("Error")
		msg = Message(top2, text="Login error")	
		msg.pack()
		button = Button(top2, text='Dismiss', command=top2.destroy)
		button.pack()
		top2.lift()

#Switch working directory to program data folder
path = os.path.expanduser('~')	#Get the path of the user's home directory
credLocation = path + "\Documents\FitbitSleep"
os.chdir(credLocation)

#Create root window and login window
root = Tk()
title_var = StringVar(root)
title_var.set("Status: Not logged in")
root.title("Fitbit Sleep Analyzer")
now = datetime.datetime.now()
top = Toplevel()

#Initialization and authentication of fitbit object
f, k = load_config()
test_token_cipher = f.readline()
token_salt = k.readline()

#Populate login window
top.title("Login")
Label(top, text = "PIN: ").grid(row = 0, column = 0, sticky = W)
e = Entry(top, width = 10, show = '*')
e.grid(row = 0, column = 1, sticky = W)
b = Button(top, text = "Login", command = lambda: login_handler(root, e, top, title_var, f, k, token_salt, test_token_cipher))
b.grid(row = 0, column = 2, sticky = W)
top.lift()

if (os.stat("token.txt").st_size == 0) or (os.stat("salt.txt").st_size == 0):		#If token.txt or salt.txt is empty, the user has never logged in before and no decryption should be attempted
	#Immediately start authorization process and destroy login window when complete to continue program
	authorize(root, top, f, k, title_var)
	top.destroy()

#Populate main window
Label(root, textvariable = title_var).grid(row = 0, column = 0, columnspan = 6, sticky = W)
Label(root, text = "Date: ").grid(row = 1, column = 0, sticky = W)
month_var = StringVar(root)
month_var.set(list(months.items())[now.month - 1][0])
month_menu = OptionMenu(root, month_var, *months)
dates_var = StringVar(root)
dates_var.set(now.day)
dates_menu = OptionMenu(root, dates_var, *dates)
years_var = StringVar(root)
years_var.set(now.year)
years_menu = OptionMenu(root, years_var, *years)

month_menu.grid(row = 1, column = 1, sticky = W)
dates_menu.grid(row = 1, column = 2, sticky = W)
years_menu.grid(row = 1, column = 3, sticky = W)

Label(root, text = "Sleep Start: ").grid(row = 2, column = 0, sticky = W, columnspan = 2)
Label(root, text = "Sleep End: ").grid(row = 3, column = 0, sticky = W, columnspan = 2)
Label(root, text = "Midsleep: ").grid(row = 4, column = 0, sticky = W, columnspan = 2)
Label(root, text = "Sleep Duration: ").grid(row = 5, column = 0, sticky = W, columnspan = 2)

start_var = StringVar(root)
end_var = StringVar(root)
midsleep_var = StringVar(root)
duration_var = StringVar(root)

Label(root, textvariable = start_var).grid(row = 2, column = 2, sticky = W, columnspan = 4)
Label(root, textvariable = end_var).grid(row = 3, column = 2, sticky = W, columnspan = 4)
Label(root, textvariable = midsleep_var).grid(row = 4, column = 2, sticky = W, columnspan = 4)
Label(root, textvariable = duration_var).grid(row = 5, column = 2, sticky = W, columnspan = 4)

download = Button(root, text = "Get", command = lambda: get_date_handler(month_var, dates_var, years_var, start_var, end_var, midsleep_var, duration_var)).grid(row = 1, column = 4)

root.mainloop()

#Program exit and cleanup
f.close()
k.close()
gc.collect()	#Call garbage collector immediately before exit in an attempt to flush sensitive objects from memory